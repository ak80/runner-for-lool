FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
            python3-lxml libcap-dev python3-polib libpam-dev libpoco-dev \
            procps inotify-tools build-essential devscripts libcap2-bin libkrb5-dev cpio \
    && apt-get build-dep -y libreoffice \
    && apt-get install -y python-polib \
    && apt-get install -y wget sudo git lsb-release


RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash

RUN export NVM_DIR="$HOME/.nvm" \
    && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  \ 
    && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  \
    && nvm install node

RUN useradd -m -s /bin/bash -G sudo lool

RUN echo 'export NVM_DIR="$HOME/.nvm" \
    && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  \
    && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  \' >> /home/lool/.bashrc


